This tool provides a graph showing the breakdown of students per program.
It uses an input csv file generated from the Student List section in Omnivox.
Note, you must include the Program Code information when saving the CSV in Lea.

# Setup:
1. Have Python 3.7 or greater installed
2. Run pip install -r requirements.txt to ensure you have all the dependencies

# Usage:
python ProgramCodes my_csv_file.csv saved_graph.png

Note, the output can be either a png, svg, or pdf. Simply supply the right extension in your file path.

# Example
![](sample/sample.png)