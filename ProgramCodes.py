import argparse
import pathlib
import os
import csv
from collections import Counter, OrderedDict
import numpy as np
import matplotlib.pyplot as plt

def get_broadcodes(broad_codes_path):
    broad_codes = {}
    with open(broad_codes_path) as codes_file:
        csv_reader = csv.reader(codes_file, delimiter=',', quotechar='"')
        for row in csv_reader:
            broad_codes[row[0]] = row[1]
    return broad_codes

def get_codes_from_csv(file_path):
    program_codes = []
    with open(file_path.resolve()) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',', quotechar='"')
        for row in csv_reader:
            program_codes.append(row["Prog."].strip("=").replace('"', ''))
    return program_codes

def get_program_names(broad_codes, program_codes):
    program_names = []
    # Replace program codes with meaningful names
    for code in program_codes:
        program_number = code.split('.')[0]
        if not program_number in broad_codes.keys():
            raise Exception(f"Couldn't find code {program_number}")
        program_name = broad_codes[program_number]
        program_names.append(program_name)
    return program_names

def generate_plot(file_name, hist, output):
    # Count unique program names
    hist = OrderedDict(hist.most_common())
    plt.barh(list(hist.keys()), list(hist.values()))
    plt.title(f"{file_name}")
    plt.xlabel("Number of Students")
    plt.tight_layout()
    plt.savefig(output)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", help="Path to the csv file to be parsed")
    parser.add_argument("output_file", help="Path where the graph should be saved,\
         note the extension will be used to determine the format")
    args = parser.parse_args()

    broad_codes_path = os.path.join(pathlib.Path(__file__).parent.resolve(), "BroadCodes.txt")
    if not os.path.exists(broad_codes_path):
        print("Error can't find BroadCodes.txt, ensure it is next to the script")
        exit(-1)
    
    broad_codes = get_broadcodes(broad_codes_path)
    # Load program codes from list of students
    file_path = pathlib.Path(args.csv_file)
    if not os.path.exists(file_path.resolve()):
        print("Can't find the specified csv file {file_path.resolve()}")
        exit(-1)
    file_name = file_path.name
    program_codes = get_codes_from_csv(file_path)
    try:
        program_names = get_program_names(broad_codes, program_codes)
    except Exception as e:
        print(e)
    hist = Counter(program_names)
    generate_plot(file_name, hist, args.output_file)
    print(f"File saved to {args.output_file}")